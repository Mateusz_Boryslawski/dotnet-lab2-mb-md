﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class Cell
    {
        public int XPosition { get; set; }
        public int YPosition { get; set; }
        public CellState CellState { get; set; }
        public CellState FutureState { get; set; }

        public Cell(int x, int y)
        {
            XPosition = x;
            YPosition = y;
        }
    }
}