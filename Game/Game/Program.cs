﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    class Program
    {
        static void Main(string[] args)
        {
            Grid grid = new Grid(10);
            grid.PrintGrid();

            while (true)
            {
                Console.ReadKey();
                System.Console.WriteLine(" ");
                System.Console.WriteLine("###################");
                System.Console.WriteLine(" ");

                grid.RefreshGrid();
                grid.UpdateGrid();
                grid.PrintGrid();
            }


        }
    }
}
