﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Game
{
    public class Grid
    {
        public int Size { get; private set; }
        private Cell[,] cells;


        public Grid(int size)
        {
            Size = size;
            cells = new Cell[Size,Size];

            Array values = Enum.GetValues(typeof(CellState));
            Random random = new Random();

            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    Cell cell = new Cell(i, j);
                    cell.CellState = (CellState) values.GetValue(random.Next(values.Length));
                    cells[i, j] = cell;
                }
            }
        }

        public void PrintGrid()
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    char tmp;
                    tmp = cells[i, j].CellState == CellState.Alive ? 'O' : '.';

                    System.Console.Write(tmp);
                    System.Console.Write(' ');
                }
                System.Console.Write('\n');
            }
        }

        public int CheckHowManyNeighbours(int x, int y)
        {
            int count = 0;

            //left
            if (x != 0)
            {
                if (cells[x - 1,y].CellState == CellState.Alive)
                    count++;
            }

            //top
            if (y != 0)
            {
                if (cells[x, y - 1].CellState == CellState.Alive)
                    count++;
            }

            //right
            if (x != Size - 1)
            {
                if (cells[x + 1, y].CellState == CellState.Alive)
                    count++;
            }

            //bottom
            if (y != Size - 1)
            {
                if (cells[x, y + 1].CellState == CellState.Alive)
                    count++;
            }

            //left top
            if (x != 0 && y != 0)
            {
                if (cells[x - 1, y - 1].CellState == CellState.Alive)
                    count++;
            }

            //right top
            if (x != Size - 1 && y != 0)
            {
                if (cells[x + 1, y - 1].CellState == CellState.Alive)
                    count++;
            }

            //right bottom
            if (x != Size - 1 && y != Size - 1)
            {
                if (cells[x + 1, y + 1].CellState == CellState.Alive)
                    count++;
            }

            //left bottom
            if (x != 0 && y != Size - 1)
            {
                if (cells[x - 1, y + 1].CellState == CellState.Alive)
                    count++;
            }

            return count;
        }


        public void RefreshGrid()
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    int number = CheckHowManyNeighbours(i, j);

                    if (number < 2 && cells[i,j].CellState == CellState.Alive)
                        cells[i,j].FutureState = CellState.Dead;

                    if(cells[i, j].CellState == CellState.Alive && (number == 2||number == 3))
                        cells[i,j].FutureState = CellState.Alive;
                    
                    if(cells[i,j].CellState == CellState.Alive && number > 3)
                        cells[i, j].FutureState = CellState.Dead;

                    if(cells[i,j].CellState == CellState.Dead && number ==3)
                        cells[i, j].FutureState = CellState.Alive;
                }
            }
        }

        public void UpdateGrid()
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    cells[i, j].CellState = cells[i, j].FutureState;
                }
            }
        }
    }

}